import React from 'react'
import MainImage from '../images/blog/b-1.jpg'
import SearchBar from '../SearchBar'
import Category from '../Category'
import PopularTags from '../PopularTags'
import RecentProperties from './RecentProperties'
import PropertiesListing from './PropertiesListing'
import Details from './Details'
import SearchProperties from './SearchProperties'

function MainSection() {
    return (
        <div className="inner-pages">

            <div id="wrapper">
                
                
                
            <section className="properties-right list featured portfolio blog">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-9 col-md-12 blog-pots">



                            <PropertiesListing />
                            <Details />    
                            
                            
                            
                            
                            </div>
                            <aside className="col-lg-3 col-md-12 car">
                                <div className="widget">
                                    <SearchProperties />
                                    <RecentProperties />
                                    <PopularTags />
                                </div>
                            </aside>
                        </div>
                    </div>
                </section>


                
            </div>
        </div>
    )
}

export default MainSection
