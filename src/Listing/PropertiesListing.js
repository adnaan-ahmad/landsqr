import React from 'react'

const PropertiesListing = () => {
    return (
        
        <div className="block-heading">
        <div className="row">
            <div className="col-lg-6 col-md-5 col-2">
                <h4>
            <span className="heading-icon">
            <i className="fa fa-th-list"></i>
            </span>
            <span className="hidden-sm-down">Properties Listing</span>
        </h4>
            </div>
            <div className="col-lg-6 col-md-7 col-10 cod-pad">
                <div className="sorting-options">
                    <select className="sorting">
                        <option>Price: High to low</option>
                        <option>Price: Low to high</option>
                        <option>Sells: High to low</option>
                        <option>Sells: Low to high</option>
                    </select>
                    <a href="#" className="change-view-btn active-view-btn"><i className="fa fa-th-list"></i></a>
                    <a href="properties-grid-4.html" className="change-view-btn lde"><i className="fa fa-th-large"></i></a>
                </div>
            </div>
        </div>
    </div>
    
    )
}

export default PropertiesListing
