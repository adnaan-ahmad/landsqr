import React from 'react';

import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import MainSection from './MainSection'


const Listing = () => {
  return (
    <div className="App">
      <HeaderTop />
      <Navigation />
      <SectionHeading heading = 'Property Listing'/>
      <MainSection />
      {/* <SearchBar /> */}
      <Footer />
    </div>
  )
}

export default Listing
