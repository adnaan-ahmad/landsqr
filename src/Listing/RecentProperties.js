import React from 'react'
import house1 from "../images/feature-properties/fp-1.jpg"
import house2 from "../images/feature-properties/fp-2.jpg"
import house3 from "../images/feature-properties/fp-3.jpg"

const RecentProperties = () => {
    return (
    <div className="recent-post py-5">
        <h5 className="font-weight-bold mb-4">Recent Properties</h5>
        <div className="recent-main">
            <div className="recent-img">
                <a href="blog-details.html"><img src={house1} alt="" /></a>
                                    </div>
                <div className="info-img">
                    <a href="blog-details.html"><h6>Family Home</h6></a>
                    <p>$230,000</p>
                </div>
            </div>
            <div className="recent-main my-4">
                <div className="recent-img">
                    <a href="blog-details.html"><img src={house2} alt="" /></a>
                                    </div>
                    <div className="info-img">
                        <a href="blog-details.html"><h6>Family Home</h6></a>
                        <p>$230,000</p>
                    </div>
                </div>
                <div className="recent-main">
                    <div className="recent-img">
                        <a href="blog-details.html"><img src={house3} alt="" /></a>
                                    </div>
                        <div className="info-img">
                            <a href="blog-details.html"><h6>Family Home</h6></a>
                            <p>$230,000</p>
                        </div>
                    </div>
                </div>
    )
}

export default RecentProperties
