import React from 'react'
import house1 from './images/blog/b-1.jpg'
import house2 from './images/blog/b-2.jpg'
import house3 from './images/blog/b-3.jpg'

function RecentPosts() {

    const images = [house1, house2, house3]

    return (
        <div className="recent-post pt-5">
            <h5 className="font-weight-bold mb-4">Recent Posts</h5>
            {images.map((image) => (
            <div>
            <div className="recent-main">
                <div className="recent-img">
                    <a href="blog-details.html"><img src={image} alt="" /></a>
                </div>
                <div className="info-img">
                    <a href="blog-details.html"><h6>Real Estate</h6></a>
                    <p>May 10, 2020</p>
                </div>
            
            </div>
            <br/>
            </div>
            ))}
            
        </div>

    )
}

export default RecentPosts
