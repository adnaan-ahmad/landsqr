import React from 'react'

const Heading = () => {
    return (
        <div className="col-12">
            <div className="banner-inner">
                <h1 className="title text-center">Find Your Dream Home</h1>
                <h5 className="sub-title text-center">We Have Over Million Properties For You</h5>
            </div>
        </div>
    )
}

export default Heading
