import React from 'react'
import image1 from '../images/testimonials/ts-2.jpg'
import image2 from '../images/testimonials/ts-4.jpg'

const Testimonial = () => {
    return (
        <section className="testimonials bg-white-3">
            <div className="container">
                <div className="sec-title">
                    <h2><span>Clients </span>Testimonials</h2>
                    <p>We collect reviews from our customers.</p>
                </div>
                <div className="owl-carousel job_clientSlide"  id = 'testimonial'>
                    <div className="singleJobClinet" id = 'card'>
                        <p>
                        LandSqr made my life easy. It helped me with the search for my first ever investment i.e. 1BHK apartment in Mira Road. Thanks to the team for providing relevant tools like EMI calculator and smart search.
                        </p>
                        <div className="detailJC">
                            <span><img src={image1} alt=""/></span>
                            <h5>Gaurav Singh</h5>
                            <p>Chennai</p>
                        </div>
                    </div>
                    <div className="singleJobClinet">
                        <p>
                        LandSqr made my life easy. It helped me with the search for my first ever investment i.e. 1BHK apartment in Mira Road. Thanks to the team for providing relevant tools like EMI calculator and smart search.
                        </p>
                        <div className="detailJC">
                            <span><img src={image2} alt=""/></span>
                            <h5>Fuzail Khan</h5>
                            <p>Jaipur</p>
                        </div>
                    </div>
                    {/* <div className="singleJobClinet">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore gna a. Ut enim ad minim veniam,
                        </p>
                        <div className="detailJC">
                            <span><img src="images/testimonials/ts-3.jpg" alt=""/></span>
                            <h5>Mary Deshaw</h5>
                            <p>Chicago</p>
                        </div>
                    </div>
                    <div className="singleJobClinet">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore gna a. Ut enim ad minim veniam,
                        </p>
                        <div className="detailJC">
                            <span><img src="images/testimonials/ts-4.jpg" alt=""/></span>
                            <h5>Gary Steven</h5>
                            <p>Philadelphia</p>
                        </div>
                    </div>
                    <div className="singleJobClinet">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore gna a. Ut enim ad minim veniam,
                        </p>
                        <div className="detailJC">
                            <span><img src="images/testimonials/ts-5.jpg" alt=""/></span>
                            <h5>Cristy Mayer</h5>
                            <p>San Francisco</p>
                        </div>
                    </div>
                    <div className="singleJobClinet">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore gna a. Ut enim ad minim veniam,
                        </p>
                        <div className="detailJC">
                            <span><img src="images/testimonials/ts-6.jpg" alt=""/></span>
                            <h5>Ichiro Tasaka</h5>
                            <p>Houston</p>
                        </div>
                    </div> */}



                </div>
            </div>
        </section>
    )
}

export default Testimonial
