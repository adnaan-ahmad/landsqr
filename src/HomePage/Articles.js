import React, { useEffect, useState } from 'react'
import image1 from '../images/feature-properties/fp-10.jpg'
import image2 from '../images/blog/b-11.jpg'
import image3 from '../images/blog/b-12.jpg'
import image4 from '../images/testimonials/ts-6.jpg'
import image5 from '../images/testimonials/ts-4.jpg'
import {fetchData} from '../Api/index'
import Loader from "react-loader-spinner"
import { Link } from 'react-router-dom'

const Articles = () => {

    const [content, setContent] = useState([])

        useEffect(() => {

            const fetchApi = async () => {
                let data = await fetchData()
                data = data.slice(3,6)
                setInterval(() => {
                    setContent(data)
                }, 0)
            }
            fetchApi()

        }, [])
            console.log(content)

            if (content.length === 0) {
                return (
                    <div className="inner-pages">
    
                    <div id="wrapper">
                        <section className="blog blog-section">
                            <div className="container">
                    <p>Wait a Moment...</p>
                    <Loader
                        type="TailSpin"
                        color="#00BFFF"
                        height={80}
                        width={80}
                        marginLeft={100}
                        id='loader'
                    />
                    </div>
                    
                    </section>
                    </div>
                    </div>
                )
            }

    return (
        <section className="blog-section bg-white-2">
            <div className="container">
                <div className="sec-title">
                    <h2><span>Articles &amp; </span>Tips</h2>
                    <p>Read the latest news from our blog.</p>
                </div>
                <div className="news-wrap">
                    <div className="row">
                        
                    {content.map((contents) => (
                        
                        <div className="col-xl-4 col-md-6 col-xs-12">
                            <div className="news-item">
                                <Link to='blogdetail' className="news-img-link">
                                    <div className="news-item-img">
                                        <img className="img-responsive" src={image1} alt="blog image" />
                                    </div>
                                </Link>
                                <div className="news-item-text">
                                    <Link to='blogdetail'><h3>{contents.title}</h3></Link>
                                    <div className="dates">
                                        <span className="date">April 11, 2020 &nbsp;/</span>
                                        <ul className="action-list pl-0">
                                            <li className="action-item pl-2"><i className="fa fa-heart"></i> <span>306</span></li>
                                            <li className="action-item"><i className="fa fa-comment"></i> <span>34</span></li>
                                            <li className="action-item"><i className="fa fa-share-alt"></i> <span>122</span></li>
                                        </ul>
                                    </div>
                                    <div className="news-item-descr big-news">
                                        <p>{contents.content}</p>
                                    </div>
                                    <div className="news-item-bottom">
                                        <Link to='blogdetail' className="news-link">Read more...</Link>
                                        <div className="admin">
                                            <p>By, Karl Smith</p>
                                            <img src={image4} alt="" /> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    ))}
                    
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Articles
