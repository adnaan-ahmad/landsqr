import React from 'react'
import Navigation from '../Navigation'
import Landing from './Landing'
import Footer from '../Footer'
import FeaturedProperties from '../HomePage/FeaturedProperties/FeaturedProperties'
import PopularPlaces from './PopularPlaces/PopularPlaces'
import GetStarted from './GetStarted'
import Testimonial from './Testimonial'
import Articles from './Articles'
import Partners from './Partners'
import HeaderTop from '../HeaderTop'

// import '../css/search.css'
// import '../css/fontawesome-all.min.css'
// import '../revolution/css/settings.css'
// import '../revolution/css/layers.css'
// import '../revolution/css/navigation.css'
// import '../css/magnific-popup.css'

// import '../css/slick.css'
// import './css/jquery-ui.css'
// import './css/owl.carousel.min.css'

const HomePage = () => {
    return (
        <div className="homepage-3">
            <div id="wrapper">
                {/* <HeaderTop /> */}
                <Navigation />
                <Landing />
                <FeaturedProperties />
                <PopularPlaces />
                <GetStarted />
                <Testimonial />
                <Articles />
                <Partners />
                <Footer />
            </div>
        </div>
    )
}

export default HomePage
