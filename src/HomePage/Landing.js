import React from 'react'
import Heading from './Heading'
import BuyRent from './BuyRent'
import SearchBar from './SearchBar'
import PopularSearches from './PopularSearches'

const Landing = () => {
    return (
        <div id="home" className="section welcome-area bg-overlay overflow-hidden">
            <div className="hero-main">
                <div className="container">
                    <div className="row">
                        <Heading />
                        <div className="col-12">
                            <div className="banner-search-wrap">
                                <BuyRent />
                                <SearchBar />
                            </div>
                                <PopularSearches />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Landing
