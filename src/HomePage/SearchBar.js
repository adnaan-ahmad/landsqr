import React from 'react'
import './HomePage.css'

const SearchBar = () => {

    const opts = [{ label: "Type", value: 1 }, { label: "Family House", value: 2 }, { label: "Apartment", value: 3 }, { label: "Condo", value: 4 }]

    return (

        <div className="tab-content">
            <div className="tab-pane fade show active" id="tabs_1">
                <div className="rld-main-search">
                    <div className="row">
                        <div className="col-xl-4 col-lg-6 col-md-6">
                            <div className="rld-single-input left-icon">
                                <input type="text" placeholder="Entry Landmark Location"  id ='landmark'/>
                            </div>
                        </div>

                        <div className="col-xl-2 col-lg-6 col-md-6">
                            <div className="rld-single-select">
                                <select className="select single-select" id = 'select'>
                                    <option value="1">Type</option>
                                    <option value="2">Family House</option>
                                    <option value="3">Apartment</option>
                                    <option value="3">Condo</option>
                                </select>
                            </div>
                        </div>

                        <div className="col-xl-2 col-lg-4 col-md-4">
                            <div className="rld-single-select">
                                <select className="select single-select" id = 'select'>
                                    <option value="1">Room 1</option>
                                    <option value="2">Room 2</option>
                                    <option value="3">Room 3</option>
                                    <option value="3">Any Bedrooms</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-4 col-md-4">
                            <div className="rld-single-select">
                                <select className="select single-select" id = 'select'>
                                    <option value="1">Any Price</option>
                                    <option value="2">Price 1</option>
                                    <option value="3">Price 2</option>
                                    <option value="3">Price 3</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-4 col-md-4">
                            <a className="btn btn-yellow" href="#" id = 'search'>SEARCH NOW</a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tab-pane fade" id="tabs_2">
                <div className="rld-main-search">
                    <div className="row">
                        <div className="col-xl-4 col-lg-6 col-md-6">
                            <div className="rld-single-input left-icon">
                                <input type="text" placeholder="Entry Landmark Location" />
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-6 col-md-6">
                            <div className="rld-single-select">
                                <select className="select single-select">
                                    <option value="1">All Properties</option>
                                    <option value="2">Properties 1</option>
                                    <option value="3">Properties 2</option>
                                    <option value="3">Properties 3</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-4 col-md-4">
                            <div className="rld-single-select">
                                <select className="select single-select">
                                    <option value="1">Room</option>
                                    <option value="2">Room 1</option>
                                    <option value="3">Room 2</option>
                                    <option value="3">Room 3</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-4 col-md-4">
                            <div className="rld-single-select">
                                <select className="select single-select">
                                    <option value="1">Any Price</option>
                                    <option value="2">Price 1</option>
                                    <option value="3">Price 2</option>
                                    <option value="3">Price 3</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-4 col-md-4">
                            <a className="btn btn-yellow" href="#">SEARCH NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default SearchBar
