import React, { useState } from 'react'

const BuyRent = () => {

    const [buyRent, setBuyRent] = useState('buy')

    const buy = () => {
        setBuyRent('rent')
    }

    const rent = () => {
        setBuyRent('buy')
    }

    if (buyRent === 'buy') {
        return (
            <ul onClick={buy} className="nav rld-banner-tab">
                <li className="nav-item">
                    <a className="nav-link active" data-toggle="tab" href="#tabs_2">For Buy</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" data-toggle="tab" href="#tabs_2">For Rent</a>
                </li>
            </ul>
        )
    }
    else {
        return (
            <ul onClick={rent} className="nav rld-banner-tab">
                <li className="nav-item">
                    <a className="nav-link" data-toggle="tab" href="#tabs_2">For Buy</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link active" data-toggle="tab" href="#tabs_2">For Rent</a>
                </li>

            </ul>
        )
    }
}

export default BuyRent
