import React, {useState, useEffect} from 'react'
import image1 from '../../images/popular-places/12.jpg'
import image2 from '../../images/popular-places/13.jpg'
import image3 from '../../images/popular-places/14.jpg'
import image4 from '../../images/popular-places/15.jpg'
import image5 from '../../images/popular-places/10.jpg'
import image6 from '../../images/popular-places/5.jpg'
import {fetchPropertyData} from '../../Api/index'
import Loader from "react-loader-spinner"
import { Link } from 'react-router-dom'

const PopularPlaces = () => {

    const [content, setContent] = useState([])

        useEffect(() => {

            const fetchApi = async () => {
                let data = await fetchPropertyData()
                data = data.slice(0, 6)
                setInterval(() => {
                    setContent(data)
                }, 0)
            }
            fetchApi()

        }, [])
        console.log(content)


        if (content.length === 0) {
            return (
                <div className="inner-pages">

                <div id="wrapper">
                    <section className="blog blog-section">
                        <div className="container">
                <p>Wait a Moment...</p>
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={80}
                    width={80}
                    marginLeft={100}
                    id='loader'
                />
                </div>
                
                </section>
                </div>
                </div>
            )
        }


    return (
        <section className="popular-places bg-white-2">
            <div className="container">
                <div className="sec-title">
                    <h2><span>Popular </span>Places</h2>
                    <p>Properties In Most Popular Places.</p>
                </div>
                
                
                
                <div className="row">
                
                {content.map((contents) => (
                    
                    <div className="col-lg-4 col-md-4">
                        
                        <Link to='listing' className="img-box hover-effect">
                            <img src={image2} className="img-responsive" alt="" />
                            <div className="img-box-content visible">
                                <h4>{contents.city}</h4>
                                <span>307 Properties</span>
                            </div>
                        </Link>
                    </div>
                    ))}
                    </div>
                     
                
                
            
                
            
            </div>
        </section>
    )
}

export default PopularPlaces
