import React from 'react'

const GetStarted = () => {
    return (
        <section className="info-help h17">
            <div className="container">
                <div className="row info-head">
                    <div className="col-lg-6 col-md-8 col-xs-8">
                        <div className="info-text">
                            <h3>Apartment for rent</h3>
                            <h5 className="mt-3">$6,400/month</h5>
                            <p className="pt-2">We Help you find the best places and offer near you. Bring to the table win-win survival strategies to ensure proactive domination going forward.</p>
                            <div className="inf-btn pro">
                                <a href="contact-us.html" className="btn btn-pro btn-secondary btn-lg">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-3"></div>
                </div>
            </div>
        </section>
    )
}

export default GetStarted
