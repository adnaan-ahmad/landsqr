import React, {useState, useEffect} from 'react'
import Heading from './Heading'
import Details from './Details'
import image1 from '../../images/blog/b-11.jpg'
import image2 from '../../images/blog/b-12.jpg'
import image3 from '../../images/blog/b-8.jpg'
import image4 from '../../images/blog/b-5.jpg'
import {fetchPropertyData} from '../../Api/index'
import Loader from "react-loader-spinner"
import { Link } from 'react-router-dom'

const FeaturedProperties = () => {

    const [content, setContent] = useState([])

        useEffect(() => {

            const fetchApi = async () => {
                let data = await fetchPropertyData()
                data = data.slice(3, 7)
                setInterval(() => {
                    setContent(data)
                }, 0)
            }
            fetchApi()

        }, [])
            // console.log(content)

            if (content.length === 0) {
                return (
                    <div className="inner-pages">
    
                    <div id="wrapper">
                        <section className="blog blog-section">
                            <div className="container">
                    <p>Wait a Moment...</p>
                    <Loader
                        type="TailSpin"
                        color="#00BFFF"
                        height={80}
                        width={80}
                        marginLeft={100}
                        id='loader'
                    />
                    </div>
                    
                    </section>
                    </div>
                    </div>
                )
            }

    return (
        <section className="featured portfolio bg-white-3 rec-pro">
            <div className="container-fluid">

                <div className="sec-title">
                    <h2><span>Featured </span>Properties</h2>
                    <p>We provide full service at every step.</p>
                </div>

                <div className="portfolio col-xl-12">
                    <div className="slick-lancers" id='details'>
                        
                        
                    {content.map((contents) => (
                        
                        <div className="agents-grid">
                            <div className="landscapes">
                                <Link to='listing'><div className="project-single" id='featuredProp'>
                                    <div className="project-inner project-head">
                                        <div className="homes">
                                            {/* <!-- homes img --> */}
                                            <a href="" className="homes-img">
                                                <div className="homes-tag button alt featured">Featured</div>
                                                <div className="homes-tag button alt sale">For Sale</div>
                                                <div className="homes-price">$ {contents.price}</div>
                                                <img src={image1} alt="home-1" className="img-responsive" />
                                            </a>
                                        </div>
                                        <div className="button-effect">
                                            {/* <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                            <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                            <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a> */}
                                        </div>
                                    </div>
                                    {/* <!-- homes content --> */}
                                    <div className="homes-content">
                                        {/* <!-- homes address --> */}
                                        <h3><a href="single-property-1.html">{contents.name}</a></h3>
                                        <p className="homes-address mb-3">
                                            <a href="single-property-1.html">
                                                <i className="fa fa-map-marker"></i><span>{contents.locality}</span>
                                            </a>
                                        </p>
                                        {/* <!-- homes List --> */}
                                        <ul className="homes-list clearfix">
                                            <li>
                                                <span>{contents.noOfBedrooms} Bedrooms</span>
                                            </li>
                                            <li>
                                                <span>{contents.noOfBathrooms} Bathrooms</span>
                                            </li>
                                            <li>
                                                <span>{contents.plotArea} sq ft</span>
                                            </li>
                                            <li>
                                                <span>{contents.disclaimer}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div></Link>
                            </div>
                        </div>
                        
                        ))}  
                    </div>
                </div>


             </div>
        </section>
    )
}

export default FeaturedProperties
