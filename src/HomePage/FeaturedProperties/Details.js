import React from 'react'
import image1 from '../../images/blog/b-11.jpg'
import './FeaturedProperties.css'

const Details = () => {
    return (
        <div className="portfolio col-xl-12">
            <div id='details'>
                <div className="agents-grid">
                    <div className="landscapes">
                        <div className="project-single">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button alt featured">Featured</div>
                                        <div className="homes-tag button alt sale">For Sale</div>
                                        <div className="homes-price">$9,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="people">
                        <div className="project-single">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button sale rent">For Rent</div>
                                        <div className="homes-price">$3,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="people landscapes no-pb pbp-3">
                        <div className="project-single">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button alt sale">For Sale</div>
                                        <div className="homes-price">$9,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="landscapes">
                        <div className="project-single no-mb">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button alt featured">Featured</div>
                                        <div className="homes-tag button sale rent">For Rent</div>
                                        <div className="homes-price">$3,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="properties-details.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="people">
                        <div className="project-single no-mb">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button alt sale">For Sale</div>
                                        <div className="homes-price">$9,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="people landscapes no-pb pbp-3">
                        <div className="project-single no-mb last">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button sale rent">For Rent</div>
                                        <div className="homes-price">$3,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="landscapes">
                        <div className="project-single">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button alt featured">Featured</div>
                                        <div className="homes-tag button alt sale">For Sale</div>
                                        <div className="homes-price">$9,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="agents-grid">
                    <div className="people">
                        <div className="project-single">
                            <div className="project-inner project-head">
                                <div className="homes">
                                    {/* <!-- homes img --> */}
                                    <a href="single-property-1.html" className="homes-img">
                                        <div className="homes-tag button sale rent">For Rent</div>
                                        <div className="homes-price">$3,000/mo</div>
                                        <img src={image1} alt="home-1" className="img-responsive" />
                                    </a>
                                </div>
                                <div className="button-effect">
                                    <a href="single-property-1.html" className="btn"><i className="fa fa-link"></i></a>
                                    <a href="https://www.youtube.com/watch?v=14semTlwyUY" className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                    <a href="single-property-2.html" className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                </div>
                            </div>
                            {/* <!-- homes content --> */}
                            <div className="homes-content">
                                {/* <!-- homes address --> */}
                                <h3><a href="single-property-1.html">Real House Luxury Villa</a></h3>
                                <p className="homes-address mb-3">
                                    <a href="single-property-1.html">
                                        <i className="fa fa-map-marker"></i><span>Est St, 77 - Central Park South, NYC</span>
                                    </a>
                                </p>
                                {/* <!-- homes List --> */}
                                <ul className="homes-list clearfix">
                                    <li>
                                        <span>6 Bedrooms</span>
                                    </li>
                                    <li>
                                        <span>3 Bathrooms</span>
                                    </li>
                                    <li>
                                        <span>720 sq ft</span>
                                    </li>
                                    <li>
                                        <span>2 Garages</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Details
