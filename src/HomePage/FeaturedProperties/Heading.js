import React from 'react'

const Heading = () => {
    return (
        <div className="sec-title">
            <h2><span>Featured </span>Properties</h2>
            <p>We provide full service at every step.</p>
        </div>
    )
}

export default Heading
