import React, { useState } from 'react'
import image1 from '../images/partners/11.jpg'
import image2 from '../images/partners/12.jpg'
import image3 from '../images/partners/13.jpg'
import image4 from '../images/partners/14.jpg'
import image5 from '../images/partners/15.jpg'
import image6 from '../images/partners/16.jpg'
import image7 from '../images/partners/17.jpg'
import './HomePage.css'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

const Partners = () => {

    const [startIndex, setStartIndex] = useState(0)
    const [endIndex, setEndIndex] = useState(5)
    
    const array = [<div key='1'><img src={image1} alt="" /></div>,
        <div key='2'><img src={image2} alt="" /></div>,
        <div key='3' className="owl-item"><img src={image3} alt="" /></div>,
        <div key='4' className="owl-item"><img src={image4} alt="" /></div>,
        <div key='5' className="owl-item"><img src={image5} alt="" /></div>,
        <div key='6' className="owl-item"><img src={image6} alt="" /></div>,
        
        <div key='7' className="owl-item"><img src={image7} alt="" /></div>,
        <div key='8' className="owl-item"><img src={image1} alt="" /></div>,
        <div key='9' className="owl-item"><img src={image2} alt="" /></div>,
        <div key='10' className="owl-item"><img src={image3} alt="" /></div>]

    const [workingArray, setWorkingArray] = useState(array.slice(0, 5))

    const slide = () => {
        // alert('jcccccccd')
        if(startIndex === 4) setStartIndex(0)
        else setStartIndex(startIndex + 1)
        
        if(endIndex === 9) setEndIndex(5)
        else setEndIndex(endIndex + 1)   
        setWorkingArray(array.slice(startIndex, endIndex))
    }

    return (
        
        <div className="partners bg-white-3" onClick={slide}>
        
            <div className="container">
            {/* <Carousel> */}
                <div className="owl-carousel style2" id = 'partner'>
                
                    {/* <div><img src={image1} alt="" /></div>
                    <div><img src={image2} alt="" /></div>
                    <div className="owl-item"><img src={image3} alt="" /></div>
                    <div className="owl-item"><img src={image4} alt="" /></div>
                    <div className="owl-item"><img src={image5} alt="" /></div>
                    <div className="owl-item"><img src={image6} alt="" /></div>
                    
                    <div className="owl-item"><img src={image7} alt="" /></div>
                    <div className="owl-item"><img src={image1} alt="" /></div>
                    <div className="owl-item"><img src={image2} alt="" /></div>
                    <div className="owl-item"><img src={image3} alt="" /></div> */}
                   
                    {workingArray}

                </div>
                {/* </Carousel> */}
            </div>
            
        </div>
        
    )
}

export default Partners
