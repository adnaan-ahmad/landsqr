import React from 'react'
import image1 from '../images/icons/icon-7.svg'
import image2 from '../images/icons/icon-8.svg'
import image3 from '../images/icons/icon-9.svg'
import image4 from '../images/icons/icon-10.svg'
import image5 from '../images/icons/icon-11.svg'

const PopularSearches = () => {
    return (
        <ul className="popular-searchs">
            <li>
                <a href="#">
                    <span className="box">
                        <img src={image1} width="45" height="45" alt="" />
                        <span>Family House</span>
                    </span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span className="box">
                        <img src={image2} width="45" height="45" alt="" />
                        <span>Apartment</span>
                    </span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span className="box">
                        <img src={image3} width="45" height="45" alt="" />
                        <span>Luxury House</span>
                    </span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span className="box">
                        <img src={image4} width="45" height="45" alt="" />
                        <span>Villa House</span>
                    </span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span className="box">
                        <img src={image5} width="45" height="45" alt="" />
                        <span>Town House</span>
                    </span>
                </a>
            </li>
        </ul>

    )
}

export default PopularSearches
