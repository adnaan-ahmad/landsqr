import React from 'react'
import { Link } from 'react-router-dom'

function RegisterForm() {
  return (
    <div class="inner-pages">
        <div id="wrapper">
        
        <div id="login">
            <div class="login">
                <form autocomplete="off">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" type="text" />
                        <i class="ti-user"></i>
                    </div>
                    {/* <div class="form-group">
                        <label>Your Last Name</label>
                        <input class="form-control" type="text" />
                        <i class="ti-user"></i>
                    </div> */}
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" type="email" />
                        <i class="icon_mail_alt"></i>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" type="password" id="password1" />
                        <i class="icon_lock_alt"></i>
                    </div>
                    <div class="form-group">
                        <label>Mobile Number</label>
                        <input class="form-control" type="text" id="password2" />
                        <i class="icon_lock_alt"></i>
                    </div><br/>
                    <div id="pass-info" class="clearfix"></div>
                    <a href="#0" class="btn_1 rounded full-width add_top_30">Register Now!</a>
                    <div class="text-center add_top_10">Already have an acccount? <strong><Link to='/login'>Sign In</Link></strong></div>
                </form>
            </div>
        </div>

        </div>
    </div>
  );
}

export default RegisterForm
