import React from 'react'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import RegisterForm from './RegisterForm'

function Register() {
  return (
    <div className="App">
      <HeaderTop />
      <Navigation />
      <SectionHeading heading = 'Register'/>
      <RegisterForm />
      <Footer />
    </div>
  );
}

export default Register
