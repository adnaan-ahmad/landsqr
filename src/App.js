import React from 'react'

import 'animate.css'
// import 'owl.carousel'
import './css/animate.css'
import './css/bootstrap.css'
import './css/menu.css'
import './css/styles.css'
import './css/lightcase.css'
import './css/default.css'
import './css/font-awesome.min.css'

// import './css/search.css'
// import './css/fontawesome-all.min.css'
// import './revolution/css/settings.css'
// import './revolution/css/layers.css'
// import './revolution/css/navigation.css'
// import './css/magnific-popup.css'

// import './css/slick.css'
// import './css/jquery-ui.css'
// import './css/owl.carousel.min.css'
// import './css/search.css'

import Profile from './Profile/Profile'
import HomePage from './HomePage/HomePage'
import BlogDetails from './BlogDetails/BlogDetails'
import GridLayout from './GridLayout/GridLayout'
import Listing from './Listing/Listing'
import Property from './Property/Property'
import Form from './Form/Form'
import PropertyForm from './Form/PropertyForm'
import Register from './Register/Register'
import Login from './Login/Login'
import Contact from './Contact/Contact'


import { BrowserRouter as Router, Route} from "react-router-dom"
import SellRent from './SellRent/SellRent'

const App = () => {

  return (
 
    <Router>

      <Route exact path = '/' component={HomePage} />
      
      <Route path = '/blog' component={GridLayout} />

      <Route path = '/listing' component={Listing} />

      <Route path = '/property' component={Property} />

      <Route path = '/blogdetail' component={BlogDetails} />

      <Route path = '/form' component={Form} />

      <Route path = '/propertyform' component={PropertyForm} />
      
      <Route path = '/register' component={Register} />

      <Route path = '/login' component={Login} />

      <Route path = '/contact' component={Contact} />

      <Route path = '/myprofile' component={Profile} />

      <Route path = '/sellrent' component={SellRent} />

    </Router>
  )
}

export default App
