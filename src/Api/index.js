export async function fetchData() {
    try {
        const url = 'https://landsqr.herokuapp.com/api/v1/blogs'
        let response = await fetch(url)
        let data = await response.json()
        // console.log("Blog Data:" +data)
        
        return data 
    }
    catch {
        console.log('error')
        return []
    }
}


export async function fetchPropertyData() {
    try {
        const url = 'https://landsqr.herokuapp.com/api/v1/properties'
        let response = await fetch(url)
        let propertyData = await response.json()
        // console.log("Property Data:" +propertyData)
        
        return propertyData
    }
    catch {
        console.log('error')
        return []
    }
}
