import React from 'react';
// import "https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i%7CMontserrat:600,800"
// import './webpack.config'

// import 'animate.css'
// import './css/animate.css'
// import './css/bootstrap.css'
// import './css/menu.css'
// import './css/styles.css'
// import './css/lightcase.css'
// import './css/default.css'
// import './css/font-awesome.min.css'
import Profile from './images/profile.png'
import { Link } from "react-router-dom"

//import './App.css'

function HeaderTop() {
  return (
    // <div className="App">
    <div className="inner-pages">
    {/* <title>Blog Details</title> */}
    {/* <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i%7CMontserrat:600,800" rel="stylesheet"></link> */}
    {/* <!-- START SECTION HEADINGS --> */}
    {/* <!-- Wrapper --> */}
    <div id="wrapper">
        {/* <!-- START SECTION HEADINGS -->
        <!-- Header Container
        ================================================== --> */}
        <header id="header-container">
      <div className="header-top">
                <div className="container">
                    <div className="top-info hidden-sm-down">
                        <div className="call-header">
                            <p><i className="fa fa-phone" aria-hidden="true"></i> (234) 0200 17813</p>
                        </div>
                        <div className="address-header">
                            <p><i className="fa fa-map-marker" aria-hidden="true"></i> 95 South Park Ave, USA</p>
                        </div>
                        <div className="mail-header">
                            <p><i className="fa fa-envelope" aria-hidden="true"></i> info@findhouses.com</p>
                        </div>
                    </div>
                    <div className="top-social hidden-sm-down">
                        <div className="login-wrap">
                            <ul className="d-flex">
                                <li><Link to='/login'><i className="fa fa-user"></i>Login</Link></li>
                                <li><Link to='/register'><i className="fa fa-sign-in"></i> Register</Link></li>
                            </ul>
                        </div>
                        <div className="social-icons-header">
                            <div className="social-icons">
                                <a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div className="dropdown">
                            <Link to='/myprofile'><button className="btn-dropdown" type="button" id="dropdownlang" data-toggle="dropdown" aria-haspopup="true">
                                <img id='profile' src={Profile} alt="lang" /> My Profile
                            </button></Link>
                            {/* <ul className="dropdown-menu" aria-labelledby="dropdownlang">
                                <li><img src="images/fr.png" alt="lang" />France</li>
                                <li><img src="images/de.png" alt="lang" /> German</li>
                                <li><img src="images/it.png" alt="lang" />Italy</li>
                            </ul> */}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            </header>
    </div>
    
    </div>
  );
}

export default HeaderTop
