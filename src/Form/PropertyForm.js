import React from 'react'
import '../App.css'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import Footer from '../Footer'
import SectionHeading from '../SectionHeading'
import { FormControl, NativeSelect } from '@material-ui/core'
import { Formik } from 'formik'
import Select from "react-select"
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import axios from 'axios'

class PropertyForm extends React.Component {

  // postingAs = [{ label: "Owner", value: 1 }, { label: "Agent", value: 2 }, { label: "Builder", value: 3 }]
  // propertyType = [{ label: "Sale", value: 1 }, { label: "Lease Rent", value: 2 }, { label: "PG", value: 3 }]
  status = [{ label: "AVAILABLE", value: 1 }, { label: "BOOKED", value: 2 }]
  water = [{ label: 'true', value: 1 }, { label: 'false', value: 2 }]

  // constructor(props){
  //   super(props)
  //   this.state = {
  //     file: null
  //   }
  //   this.handleChange = this.handleChange.bind(this)
  // }

  // handleChange(event) {
  //   this.setState({
  //     file: URL.createObjectURL(event.target.files[0])
  //   })
  // }

  constructor(props) {
    super(props)

    this.state = {
      // postingAs: '',
      // city: '',
      // locality: '',
      // propertyType: '',
      name: '',
      price: 0,
      owner: '5fa650443a99a76b5fe2934a',
      noOfBedrooms: 0,
      noOfBathrooms: 0,
      noOfBalcony: 0,
      noOfStoreRoom: 0,
      superArea: 0,
      pricePerSquareFit: 0,
      plotArea: 0,
      status: "AVAILABLE",
      floor: 0,
      carParking: 0,
      waterAvailability: true,
      disclaimer: '',
      description: ''
    }
  }

  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  changeStatus = e => {
    // console.log(e.label)
    this.setState({ status: e.label })
  }

  changeWaterStatus = e => {
    // console.log(e.label)
    this.setState({ water: e.label })
  }

  // changeContent = (e, editor) => {
  //   // console.log(e)
  //   const data = editor.getData()
  //   this.setState({ content : ReactHtmlParser(data)[0].props.children[0]})

  // }

  submitHandler = e => {
    e.preventDefault()

    console.log(this.state)
    axios
      .post('https://landsqr.herokuapp.com/api/v1/properties', this.state)
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const { name, price, noOfBedrooms, noOfBathrooms, noOfBalcony, noOfStoreRoom, superArea, pricePerSquareFit, plotArea, status, floor, carParking, waterAvailability, disclaimer, description } = this.state
    return (

      <div>
        <HeaderTop />
        <Navigation />
        <SectionHeading heading='Property Post' />
        <div class="inner-pages">

          <div id="wrapper">


            <section class="royal-add-property-area section_100">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">


                    <div class="single-add-property">
                      <h3>Property Post</h3>
                      <div class="property-form-group">
                        <Formik>
                          <form onSubmit={this.submitHandler}>

                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdowntype">Name</label>

                                  <input type="text" onChange={this.changeHandler} value={name} className='property-form' name="name" id="name" placeholder="Enter Property Name.." />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Price</label>

                                  <input type="number" onChange={this.changeHandler} value={price} className='property-form' name="price" id="property-price" placeholder="Enter Price.." min='0' max='99999999' />
                                </div>
                              </div>


                            </div>


                            <div class="row">

                              {/* <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdowntype">Owner</label>

                                  <input type="text" className='property-form' name="name" id="name" placeholder="Enter Owner Name.." />
                                </div>
                              </div> */}

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">No Of Bedrooms</label>

                                  <input type="number" onChange={this.changeHandler} value={noOfBedrooms} className='property-form' name="noOfBedrooms" id="property-price" placeholder="Enter No of Bedrooms.." min='0' max='99999999' />
                                </div>
                              </div>


                            </div>

                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdowntype">No of Bathrooms</label>

                                  <input type="number" onChange={this.changeHandler} value={noOfBathrooms} className='property-form' name="noOfBathrooms" id="property-price" placeholder="Enter No of Bathrooms.." min='0' />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">No Of Balcony</label>

                                  <input type="number" onChange={this.changeHandler} value={noOfBalcony} className='property-form' name="noOfBalcony" id="property-price" placeholder="Enter No of Balcony.." min='0' max='99999999' />
                                </div>
                              </div>

                            </div>


                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">No Of Store Room</label>

                                  <input type="number" onChange={this.changeHandler} value={noOfStoreRoom} className='property-form' name="noOfStoreRoom" id="property-price" placeholder="Enter No of Store Room.." min='0' max='99999999' />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdowntype">Super Area</label>

                                  <input type="number" onChange={this.changeHandler} value={superArea} className='property-form' name="superArea" id="property-price" placeholder="Enter Super Area.." min='0' />
                                </div>
                              </div>

                            </div>


                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Price Per Square Feet</label>

                                  <input type="number" onChange={this.changeHandler} value={pricePerSquareFit} className='property-form' name="pricePerSquareFit" id="property-price" placeholder="Enter Price Per Square Feet.." min='0' max='99999999' />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Plot Area</label>

                                  <input type="number" onChange={this.changeHandler} value={plotArea} className='property-form' name="plotArea" id="property-price" placeholder="Enter Plot Area.." min='0' max='99999999' />
                                </div>
                              </div>


                            </div>


                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Status</label>

                                  <Select id="country" name="country" className='post-status'
                                    isClearable={true}
                                    isSearchable={true}
                                    name="status"
                                    onChange={this.changeStatus}
                                    label={status} 
                                    options={this.status}
                                  />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Floor</label>

                                  <input type="number" value={floor} onChange={this.changeHandler} className='property-form' name="floor" id="property-price" placeholder="Enter Floor No.." min='0' max='99999999' />
                                </div>
                              </div>


                            </div>


                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Car Parking</label>

                                  <input type="number" value={carParking} onChange={this.changeHandler} className='property-form' name="carParking" id="property-price" placeholder="Enter Price Per Square Feet.." min='0' max='99999999' />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Water Availability</label>

                                  <Select id="country" name="country" className='post-status'
                                    isClearable={true}
                                    isSearchable={true}
                                    name="waterAvailability"
                                    options={this.water}
                                    label={waterAvailability}
                                    onChange={this.changeWaterStatus}
                                  />
                                </div>
                              </div>

                            </div>


                            <div class="row">

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdowntype">Disclaimer</label>

                                  <input type="text" value={disclaimer} onChange={this.changeHandler} className='property-form' name="disclaimer" id="name" placeholder="Write Disclaimer.." />
                                </div>
                              </div>

                              <div class="col-lg-6 col-md-12">
                                <div class="dropdown faq-drop">
                                  <label for="dropdownmissdcall">Description</label>

                                  <input type="text" value={description} onChange={this.changeHandler} className='property-form' name="description" id="property-price" placeholder="Enter Description.." min='0' max='99999999' />
                                </div>
                              </div>


                            </div>

                            <br />
                            <div class="add-property-button">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="prperty-submit-button">
                                    <button type="submit">Submit Property</button>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </form>
                        </Formik>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        <Footer />
      </div>

    )
  }
}

export default PropertyForm
