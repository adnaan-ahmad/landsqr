import React from 'react'
import '../App.css'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import Footer from '../Footer'
import SectionHeading from '../SectionHeading'
import { FormControl, NativeSelect } from '@material-ui/core'
import { Formik } from 'formik'
import Select from "react-select"
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import axios from 'axios'
import ReactHtmlParser from 'react-html-parser'

class Form extends React.Component {

  blogStatus = [{ label: "DRAFT", value: 1 }, { label: "PUBLISHED", value: 2 }]
  tags = [{ label: "tag1", value: 'tag1' }]

  // constructor(props){
  //   super(props)
  //   this.state = {
  //     file: null
  //   }
  //   this.handleChange = this.handleChange.bind(this)
  // }

  // handleChange(event) {
  //   this.setState({
  //     file: URL.createObjectURL(event.target.files[0])
  //   })
  // }

  constructor(props) {
    super(props)

    this.state = {
      title: '',
      summary: '',
      content: '',
      postStatus: '',
      tags: '',
      blogWriter: '5fa646426a92833ef9799ca3'
    }
  }

  changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
	}

  changeTags = e => {
    // console.log(e.label)
    this.setState({ tags: e.label })
  }

  changePostStatus = e => {
    // console.log(e.label)
    this.setState({ postStatus: e.label })
  }

  changeContent = (e, editor) => {
    // console.log(e)
    const data = editor.getData()
    // console.log(ReactHtmlParser(data)[0].props.children[0])
    this.setState({ content : ReactHtmlParser(data)[0].props.children[0]})

  }

  submitHandler = e => {
    e.preventDefault()
    
    console.log(this.state)
    axios
      .post('https://landsqr.herokuapp.com/api/v1/blogs/post', this.state)
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const { title, summary, content, postStatus, tags } = this.state
    return (

      <div>
        <HeaderTop />
        <Navigation />
        <SectionHeading heading='Blog Post' />
        <div class="inner-pages">

          <div id="wrapper">


            <section class="royal-add-property-area section_100">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">


                    <div class="single-add-property">
                      <h3>Blog Post</h3>
                      <div class="property-form-group">
                        <Formik>
                        <form onSubmit={this.submitHandler}>
                          <div class="row">
                            <div class="col-md-12">
                              <p>
                                <label for="title">Title</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={title} name="title" id="title" placeholder="Enter your property title.." />
                              </p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <p>
                                <label for="title">Summary</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={summary} name="summary" id="title" placeholder="Write your property summary.." />
                              </p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <p>
                                <label for="description">Content</label>
                                {/* <CKEditor
                                  id="editor"
                                  editor={ClassicEditor}
                                  value={content}
                                  name='content'
                                  onChange={this.changeContent}
                                /> */}
                                <textarea onChange={this.changeHandler} value={content} name='content' className='blog-form' id="description" placeholder="Describe about your property.."></textarea>
                              </p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6 col-md-12">
                              <div class="dropdown faq-drop">
                                <label for="dropdownmissdcall">Post Status</label>

                                <Select
                                  id="country"
                                  className='post-status'
                                  isClearable={true}
                                  isSearchable={true}
                                  name="postStatus"
                                  options={this.blogStatus}
                                  label={postStatus}
                                  onChange={this.changePostStatus}
                                />
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                              <div class="dropdown faq-drop">
                                <label for="dropdowntype">Tags</label>

                                <Select
                                  id="country"
                                  name="tags"
                                  isClearable={true}
                                  isSearchable={true}

                                  options={this.tags}
                                  label={tags}
                                  onChange={this.changeTags}
                                />
                              </div>
                            </div>

                          </div>

                          {/* <div class="row">
                            <div class="col-lg-12 col-md-12">
                              <p class="no-mb">
                                <label for="price">Blog Image :</label>
                                
                                <input type="file" onChange={this.handleChange} name="price" className="filetype" id="price"/><br/><br/>
                                <img id="target" src={this.state.file} id='formImage' />
                              </p>
                            </div>
                            
                          </div> */}
                          <br/>
                          <div class="add-property-button">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="prperty-submit-button">
                                  <button type="submit">Submit Blog Post</button>
                                </div>
                              </div>
                            </div>
                          </div>

                        </form>
                        </Formik>
                      </div>
                    </div>


                    {/* <div class="add-property-button">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="prperty-submit-button">
                          <button type="submit">Submit Blog Post</button>
                        </div>
                      </div>
                    </div>
                  </div> */}

                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        <Footer />
      </div>

    )
  }
}

export default Form
