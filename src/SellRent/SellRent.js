import React from 'react'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import SellRentForm from './SellRentForm'

function SellRent() {
  return (
    <div className="App">
      <HeaderTop />
      <Navigation />
      <SectionHeading heading = 'Sell / Rent'/>
      <SellRentForm />
      <Footer />
    </div>
  );
}

export default SellRent
