import React from 'react'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import LoginForm from './LoginForm'

function Register() {
  return (
    <div className="App">
      <HeaderTop />
      <Navigation />
      <SectionHeading heading = 'Login'/>
      <LoginForm />
      <Footer />
    </div>
  );
}

export default Register
