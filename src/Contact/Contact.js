import React from 'react'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import Location from './Location'
import ContactUs from './ContactUs'

function Contact() {
    return (
        <div className="App">
            <HeaderTop />
            <Navigation />
            <SectionHeading heading='Contact Us' />
            <div class="inner-pages">
                <div id="wrapper">
                    <section class="contact-us">
                        <div class="container">
                            <Location />
                            <ContactUs />
                        </div>
                    </section>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Contact
