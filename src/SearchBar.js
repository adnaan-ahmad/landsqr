import React from 'react'
import './App.css'

function SearchBar() {
    return (
        // <div classNameName="inner-pages">

        // <div id="wrapper">
        // <section className="blog blog-section bg-white">
        // <div className="container">
        // <div className="row">
        // <aside className="col-lg-3 col-md-12">
        //     <div className="widget">
        <div>
                <h5 className="font-weight-bold mb-4">Search</h5>
                <div className="input-group">
                    <input type="text" className="form-control" placeholder="Search for..." id='searchBar'/>
                    <span className="input-group-btn">
                        <button className="btn btn-primary" type="button"><i className="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                </div>
        </div>
        // </aside>
        // </div>
        // </div>
        // </section>
        // </div>
        // </div>
    )
}

export default SearchBar
