import React from 'react';
//import './App.css';
import Logo from './images/logo.svg'

import { NavLink } from "react-router-dom"

const Navigation = () => {
  return (
    
    <div className="inner-pages">
    {/* <!-- START SECTION HEADINGS -->
    <!-- Wrapper --> */}
    <div id="wrapper">
        {/* <!-- START SECTION HEADINGS -->
        <!-- Header Container
        ================================================== --> */}
        <header id="header-container">

            <div id="header">
                <div className="container">
                    {/* <!-- Left Side Content --> */}
                    <div className="left-side">
                        {/* <!-- Logo --> */}
                        <div id="logo">
                            <a href="index.html"><img src={Logo} alt="" /></a>
                        </div>
                        {/* <!-- Mobile Navigation --> */}
                        <div className="mmenu-trigger">
                            <button className="hamburger hamburger--collapse" type="button">
                                <span className="hamburger-box">
							<span className="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                        
                        {/* <!-- Main Navigation --> */}
                        <nav id="navigation" className="style-1">
                            <ul id="responsive" class='unordered-list'>
                            <li><NavLink to='/'>Home</NavLink></li>
                                    {/* <ul>
                                        <li><NavLink to='/'>Home</NavLink> */}
                                            {/* <ul>
                                                <li><a href="index.html">Home Map Style 1</a></li>
                                                <li><a href="index-9.html">Home Map Style 2</a></li>
                                                <li><a href="index-12.html">Home Map Style 3</a></li>
                                            </ul> */}
                                        {/* </li> */}
                                        {/* <li><a href="#">Home Image</a>
                                            <ul>
                                                <li><a href="index-2.html">Home Boxed Image</a></li>
                                                <li><NavLink to='/'>Home Modern Image</NavLink></li>
                                                <li><a href="index-5.html">Home Minimalist Style</a></li>
                                                <li><a href="index-6.html">Home Parallax Image</a></li>
                                                <li><a href="index-15.html">Home Typed Image</a></li>
                                                <li><a href="index-17.html">Modern Parallax Image</a></li>
                                                <li><a href="index-18.html">Image Filter Search</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="#">Home Video</a>
                                                <ul>
                                                    <li><a href="index-4.html">Home Video Image</a></li>
                                                    <li><a href="index-7.html">Home Video</a></li>
                                                    <li><a href="index-20.html">Home Modern Video</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Home Slider</a>
                                                <ul>
                                                    <li><a href="index-8.html">Home Full Slider</a></li>
                                                    <li><a href="index-10.html">Home Image Slider</a></li>
                                                    <li><a href="index-11.html">Slider Presentation 2</a></li>
                                                    <li><a href="index-16.html">Slider Presentation 3</a></li>
                                                    <li><a href="index-19.html">Home Modern Slider</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Home Styles</a>
                                                <ul>
                                                    <li><a href="index-13.html">Home Style Dark</a></li>
                                                    <li><a href="index-14.html">Home Style White</a></li>
                                                </ul>
                                            </li> */}
                                    {/* </ul>
                                    </li> */}
                                    {/* <li><a href="#">Listing</a>
                                        <ul> */}
                                        <li><NavLink to='/listing'>Listing</NavLink></li>
                                                {/* <ul>
                                                    <li><a href="properties-grid-1.html">Grid View 1</a></li>
                                                    <li><a href="properties-grid-2.html">Grid View 2</a></li>
                                                    <li><a href="properties-grid-3.html">Grid View 3</a></li>
                                                    <li><a href="properties-grid-4.html">Grid View 4</a></li>
                                                    <li><a href="properties-full-grid-1.html">Grid Fullwidth 1</a></li>
                                                    <li><a href="properties-full-grid-2.html">Grid Fullwidth 2</a></li>
                                                    <li><a href="properties-full-grid-3.html">Grid Fullwidth 3</a></li>
                                                </ul> */}
                                            {/* </li>
                                            <li><NavLink to='/propertyform'>Property Form</NavLink></li> */}
                                            {/* <li><a href="#">Listing List</a>
                                                <ul>
                                                    <li><a href="properties-full-list-1.html">List View 1</a></li>
                                                    <li><a href="properties-list-1.html">List View 2</a></li>
                                                    <li><a href="properties-full-list-2.html">List View 3</a></li>
                                                    <li><NavLink to='/listing'>List View 4</NavLink></li>
                                                </ul>
                                            </li> */}
                                            {/* <li><a href="#">Listing Map</a>
                                                <ul>
                                                    <li><a href="properties-half-map-1.html">Half Map 1</a></li>
                                                    <li><a href="properties-half-map-2.html">Half Map 2</a></li>
                                                    <li><a href="properties-half-map-3.html">Half Map 3</a></li>
                                                    <li><a href="properties-top-map-1.html">Top Map 1</a></li>
                                                    <li><a href="properties-top-map-2.html">Top Map 2</a></li>
                                                    <li><a href="properties-top-map-3.html">Top Map 3</a></li>
                                                </ul>
                                            </li> */}
                                        {/* </ul> */}
                                    {/* </li> */}
                                    {/* <li><a href="#">Property</a>
                                        <ul> */}
                                            {/* <li><a href="single-property-1.html">Single Property 1</a></li>
                                            <li><a href="single-property-2.html">Single Property 2</a></li>
                                            <li><a href="single-property-3.html">Single Property 3</a></li>
                                            <li><a href="single-property-4.html">Single Property 4</a></li>
                                            <li><a href="single-property-5.html">Single Property 5</a></li> */}
                                            <li><NavLink to='/property'>Property</NavLink></li>
                                            
                                        {/* </ul>
                                    </li> */}
                                    {/* <li><a href="#">Agent</a>
                                        <ul>
                                            <li><a href="#">Agent View</a>
                                                <ul>
                                                    <li><a href="agents-listing-grid.html">Agent View 1</a></li>
                                                    <li><a href="agents-listing-row.html">Agent View 2</a></li>
                                                    <li><a href="agents-listing-row-2.html">Agent View 3</a></li>
                                                    <li><a href="agent-details.html">Agent Details</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Agencies View</a>
                                                <ul>
                                                    <li><a href="agencies-listing-1.html">Agencies View 1</a></li>
                                                    <li><a href="agencies-listing-2.html">Agencies View 2</a></li>
                                                    <li><a href="agencies-details.html">Agencies Details</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li> */}


                                    {/* <li><a href="#">Pages</a>
                                        <ul>
                                           <li><a href="#">Shop</a>
                                                <ul>
                                                    <li><a href="shop-with-sidebar.html">Product Sidebar</a></li>
                                                    <li><a href="shop-full-page.html">Product Fullpage</a></li>
                                                    <li><a href="shop-single.html">Product Single</a></li>
                                                    <li><a href="shop-checkout.html">Checkout Page</a></li>
                                                    <li><a href="shop-order.html">Order Page</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">User Panel</a>
                                                <ul>
                                                    <li><a href="dashboard.html">Dashboard</a></li>
                                                    <li><a href="user-profile.html">User Profile</a></li>
                                                    <li><a href="my-listings.html">My Properties</a></li>
                                                    <li><a href="favorited-listings.html">Favorited Properties</a></li>
                                                    <li><a href="add-property.html">Add Property</a></li>
                                                    <li><a href="payment-method.html">Payment Method</a></li>
                                                    <li><a href="invoice.html">Invoice</a></li>
                                                    <li><a href="change-password.html">Change Password</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="about.html">About Us</a></li>                                            
                                            <li><a href="faq.html">Faq</a></li>
                                            <li><a href="pricing-table.html">Pricing Tables</a></li>
                                            <li><a href="404.html">Page 404</a></li>
                                            <li><a href="login.html">Login</a></li>
                                            <li><a href="register.html">Register</a></li>
                                            <li><a href="coming-soon.html">Coming Soon</a></li>
                                            <li><a href="under-construction.html">Under Construction</a></li>
                                            <li><a href="ui-element.html">UI Elements</a></li>
                                        </ul>
                                    </li> */}
                                    
                                    
                                    
                                    
                                    <li><a href="#">Blog</a>
                                        <ul>
                                        <li><NavLink to='/blog'>Blog</NavLink>
                                                {/* <ul>
                                                    <li><a href="blog-full-grid.html">Full Grid</a></li>
                                                    <li><a href="blog-grid-sidebar.html">With Sidebar</a></li>
                                                </ul> */}
                                            </li>
                                            <li><NavLink to='/blogdetail'>Blog Details</NavLink>
                                                {/* <ul>
                                                    <li><a href="blog-full-list.html">Full List</a></li>
                                                    <li><NavLink to='/blog'>With Sidebar</NavLink></li>
                                                </ul> */}
                                            </li>
                                            {/* <li><NavLink to='/blogdetail'>Blog Details</NavLink></li> */}
                                            <li><NavLink to='/form'>Blog Post</NavLink></li>
                                        </ul>
                                    </li>
                                    <li><NavLink to='/contact'>Contact</NavLink>
                                    </li>
                                    <li><NavLink to='/sellrent'>Sell/ Rent</NavLink>
                                    </li>
                            </ul>
                        </nav>
                        <div className="clearfix"></div>
                        {/* <!-- Main Navigation / End --> */}
                    </div>
                    {/* <!-- Left Side Content / End --> */}

                    {/* <!-- Right Side Content / End --> */}
                    <div className="right-side hidden-lg-down">
                        {/* <!-- Header Widget --> */}
                        <div className="header-widget">
                            <NavLink to='/propertyform' className="button border">Submit Property</NavLink>
                        </div>
                        {/* <!-- Header Widget / End --> */}
                    </div>
                    {/* <!-- Right Side Content / End --> */}

                </div>
            </div>

        </header>
        <div className="clearfix"></div>

    </div>
    </div>

  )
}

export default Navigation
