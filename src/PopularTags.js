import React from 'react'

function PopularTags() {
    return (
        <div className="recent-post">
            <h5 className="font-weight-bold mb-4">Popular Tags</h5>
            <div className="tags">
                <span><a href="#" className="btn btn-outline-primary">Houses</a></span>
                <span><a href="#" className="btn btn-outline-primary">Real Home</a></span>
            </div>
            <div className="tags">
                <span><a href="#" className="btn btn-outline-primary">Baths</a></span>
                <span><a href="#" className="btn btn-outline-primary">Beds</a></span>
            </div>
            <div className="tags">
                <span><a href="#" className="btn btn-outline-primary">Garages</a></span>
                <span><a href="#" className="btn btn-outline-primary">Family</a></span>
            </div>
            <div className="tags">
                <span><a href="#" className="btn btn-outline-primary">Real Estates</a></span>
                <span><a href="#" className="btn btn-outline-primary">Properties</a></span>
            </div>
            <div className="tags">
                <span><a href="#" className="btn btn-outline-primary">Location</a></span>
                <span><a href="#" className="btn btn-outline-primary">Price</a></span>
            </div>
        </div>
    )
}

export default PopularTags
