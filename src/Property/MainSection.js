import React from 'react'
import Landing from './Landing'
import Gallery from './Gallery'
import SimilarProperties from './SimilarProperties'
import MiddleSection from './MiddleSection'

const MainSection = () => {
    return (
        <div className="inner-pages">
            <div id="wrapper">
                <Landing />
                <section className="single-proper blog details">
                <div className="container">
                    <MiddleSection />
                    <br/>
                    <SimilarProperties />
                </div>
                </section>
            </div>
        </div>
    )
}

export default MainSection
