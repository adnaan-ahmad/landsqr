import React from 'react'
import image from '../images/blog/b-2.jpg'

const PropertyVideo = () => {
    return (
        <div className="property wprt-image-video w50 pro vid-si6">
            <h5>Property Video</h5>
            <img alt="image" src={image} />
            <a className="icon-wrap popup-video popup-youtube" href="https://www.youtube.com/watch?v=14semTlwyUY">
                <i className="fa fa-play"></i>
            </a>
            <div className="iq-waves">
                <div className="waves wave-1"></div>
                <div className="waves wave-2"></div>
                <div className="waves wave-3"></div>
            </div>
        </div>
    )
}

export default PropertyVideo
