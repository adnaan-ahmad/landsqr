import React from 'react'

const Landing = () => {
    return (
        <div>
            <section className="breadcrumb-outer">
                <div className="container">
                    <div className="detail-title">
                        <div className="detail-title-inner">
                            <div className="row">
                                <div className="col-lg-7 col-md-12 col-sm-12">
                                    <div className="listing-rating">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star-half"></i>
                                    </div>
                                    <h2 className="white"><span>Apartments in Downtown</span></h2>
                                    <div className="list-single-contacts">
                                        <ul>
                                            <li className="white"><i className="fa fa-phone mr-2"></i><a href="#" className="white"> (234) 0200 17813</a></li>
                                            <li className="white"><i className="fa fa-map-marker mr-2"></i><a href="#" className="white">95 South Park Ave, USA</a></li>
                                            <li className="white the-last"><i className="fa fa-envelope mr-2"></i><a href="#" className="white">info@findhouses.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-lg-5 col-md-12 col-sm-12">
                                    <div className="list-single-details text-right">
                                        <div className="list-single-rating w-100">
                                            <div className="rating-score d-flex align-items-center float-right">
                                                <div className="pr-3 white"><strong>Very Good</strong>
                                                    <br/>62 Reviews </div>
                                                    <span>4.5</span>
                                                </div>
                                            </div>
                                            <div className="list-single-links w-100 mt-1">
                                                <a className="nir-btn nir-btn1 mr-3" href="#">For Sale:&nbsp; $230,000</a>
                                                <a className="nir-btn-black nir-btn1" href="#"> $1,200 / sq ft</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
    )
}

export default Landing
