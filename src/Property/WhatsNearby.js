import React from 'react'

const WhatsNearby = () => {
    return (
        <div className="floor-plan property wprt-image-video w50 pro">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-
awesome.min.css" rel="stylesheet" integrity="sha384-
wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" 
crossOrigin="anonymous"></link>
            <h5>What's Nearby</h5>
            <div className="property-nearby">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="nearby-info mb-4">
                            <span className="nearby-title mb-3 d-block text-info">
                                <i className="fa fa-graduation-cap mr-2"></i><b className="title">Education</b>
                            </span>
                            <div className="nearby-list">
                                <ul className="property-list list-unstyled mb-0">
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">Education Mandarin</h6>
                                        <span>(15.61 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">Marry's Education</h6>
                                        <span>(15.23 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">The Kaplan</h6>
                                        <span>(15.16 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="nearby-info mb-4">
                            <span className="nearby-title mb-3 d-block text-success">
                                <i className="fa fa-user-md mr-2"></i><b className="title">Health & Medical</b>
                            </span>
                            <div className="nearby-list">
                                <ul className="property-list list-unstyled mb-0">
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">Natural Market</h6>
                                        <span>(13.20 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">Food For Health</h6>
                                        <span>(13.22 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">A Matter of Health</h6>
                                        <span>(13.34 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="nearby-info">
                            <span className="nearby-title mb-3 d-block text-danger">
                                <i className="fa fa-car mr-2"></i><b className="title">Transportation</b>
                            </span>
                            <div className="nearby-list">
                                <ul className="property-list list-unstyled mb-0">
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">Airport Transportation</h6>
                                        <span>(11.36 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">NYC Executive Limo</h6>
                                        <span>(11.87 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                    <li className="d-flex">
                                        <h6 className="mb-3 mr-2">Empire Limousine</h6>
                                        <span>(11.52 miles)</span>
                                        <ul className="list-unstyled list-inline ml-auto">
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                            <li className="list-inline-item m-0 text-warning"><i className="fa fa-star fa-xs"></i></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WhatsNearby
