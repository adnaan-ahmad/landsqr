import React, {useState} from 'react'
import image1 from '../images/single-property/s-1.jpg'
import image2 from '../images/single-property/s-2.jpg'
import image3 from '../images/single-property/s-3.jpg'

import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'

const Gallery = () => {

    const image = [
        <div className="active item carousel-item" data-slide-number="0">
            <img src={image1} className="img-fluid" alt="slider-listing" />
        </div>,
        <div className="active item carousel-item" data-slide-number="1">
            <img src={image2} className="img-fluid" alt="slider-listing" />
        </div>,
        <div className="active item carousel-item" data-slide-number="2">
            <img src={image3} className="img-fluid" alt="slider-listing" />
        </div>,
        <div className="active item carousel-item" data-slide-number="4">
            <img src={image1} className="img-fluid" alt="slider-listing" />
        </div>,
        <div className="active item carousel-item" data-slide-number="5">
            <img src={image2} className="img-fluid" alt="slider-listing" />
        </div>
    ]

    const [index, setIndex] = useState(1)

    const [currentImage, setCurrentImage] = useState([<div className="active item carousel-item" data-slide-number="0">
        <img src={image1} className="img-fluid" alt="slider-listing" />
    </div>])

    
    const slideLeft = () => {
        
        setCurrentImage(image[index])
        if(index === 4) setIndex(0)
        else setIndex(index + 1)
    }

    const slideRight = () => {
        
        setCurrentImage(image[index])
        if(index === 0) setIndex(4)
        else setIndex(index - 1)
    }

    return (
        <div id="listingDetailsSlider" className="carousel listing-details-sliders slide mb-30">
            <h5 className="mb-4">Gallery</h5>
            <div className="carousel-inner">
            {/* <Slide> */}

            {currentImage}
                {/* <div className="active item carousel-item" data-slide-number="0">
                    <img src={image1} className="img-fluid" alt="slider-listing" />
                </div>
                <div className="item carousel-item" data-slide-number="1">
                    <img src={image2} className="img-fluid" alt="slider-listing" />
                </div>
                <div className="item carousel-item" data-slide-number="2">
                    <img src={image3} className="img-fluid" alt="slider-listing" />
                </div>
                <div className="item carousel-item" data-slide-number="4">
                    <img src={image1} className="img-fluid" alt="slider-listing" />
                </div>
                <div className="item carousel-item" data-slide-number="5">
                    <img src={image2} className="img-fluid" alt="slider-listing" />
                </div> */}
            {/* </Slide> */}
                <a onClick={slideLeft} className="carousel-control left" data-slide="prev"><i className="fa fa-angle-left"></i></a>
                <a onClick={slideRight} className="carousel-control right" data-slide="next"><i className="fa fa-angle-right"></i></a>

            </div>
            {/* <!-- main slider carousel nav controls --> */}
            <ul className="carousel-indicators smail-listing list-inline nav nav-justified">
                <li className="list-inline-item active">
                    <a id="carousel-selector-0" className="selected" data-slide-to="0" data-target="#listingDetailsSlider">
                        <img src={image1} className="img-fluid" alt="listing-small" />
                    </a>
                </li>
                <li className="list-inline-item">
                    <a id="carousel-selector-1" data-slide-to="1" data-target="#listingDetailsSlider">
                        <img src={image2} className="img-fluid" alt="listing-small" />
                    </a>
                </li>
                <li className="list-inline-item">
                    <a id="carousel-selector-2" data-slide-to="2" data-target="#listingDetailsSlider">
                        <img src={image3} className="img-fluid" alt="listing-small" />
                    </a>
                </li>
                <li className="list-inline-item">
                    <a id="carousel-selector-3" data-slide-to="3" data-target="#listingDetailsSlider">
                        <img src={image1} className="img-fluid" alt="listing-small" />
                    </a>
                </li>
                <li className="list-inline-item">
                    <a id="carousel-selector-4" data-slide-to="4" data-target="#listingDetailsSlider">
                        <img src={image2} className="img-fluid" alt="listing-small" />
                    </a>
                </li>
            </ul>
            {/* <!-- main slider carousel items --> */}
        </div>
    )
}

export default Gallery
