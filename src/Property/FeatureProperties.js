import React from 'react'
import image1 from '../images/feature-properties/fp-1.jpg'
import image2 from '../images/feature-properties/fp-2.jpg'
import image3 from '../images/feature-properties/fp-3.jpg'
import image4 from '../images/feature-properties/fp-4.jpg'
import image5 from '../images/feature-properties/fp-5.jpg'
import image6 from '../images/feature-properties/fp-6.jpg'

const FeatureProperties = () => {
    return (
        <div className="widget-boxed mt-5">
            <div className="widget-boxed-header mb-5">
                <h4>Feature Properties</h4>
            </div>
            <div className="widget-boxed-body">
                <div className="slick-lancers">
                    <div className="agents-grid mr-0">
                        <div className="listing-item compact">
                            <a href="properties-details.html" className="listing-img-container">
                                <div className="listing-badges">
                                    <span className="featured">$ 230,000</span>
                                    <span>For Sale</span>
                                </div>
                                <div className="listing-img-content">
                                    <span className="listing-compact-title">House Luxury <i>New York</i></span>
                                    <ul className="listing-hidden-content">
                                        <li>Area <span>720 sq ft</span></li>
                                        <li>Rooms <span>6</span></li>
                                        <li>Beds <span>2</span></li>
                                        <li>Baths <span>3</span></li>
                                    </ul>
                                </div>
                                <img src={image1} alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="agents-grid mr-0">
                        <div className="listing-item compact">
                            <a href="properties-details.html" className="listing-img-container">
                                <div className="listing-badges">
                                    <span className="featured">$ 6,500</span>
                                    <span className="rent">For Rent</span>
                                </div>
                                <div className="listing-img-content">
                                    <span className="listing-compact-title">House Luxury <i>Los Angles</i></span>
                                    <ul className="listing-hidden-content">
                                        <li>Area <span>720 sq ft</span></li>
                                        <li>Rooms <span>6</span></li>
                                        <li>Beds <span>2</span></li>
                                        <li>Baths <span>3</span></li>
                                    </ul>
                                </div>
                                <img src={image2} alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="agents-grid mr-0">
                        <div className="listing-item compact">
                            <a href="properties-details.html" className="listing-img-container">
                                <div className="listing-badges">
                                    <span className="featured">$ 230,000</span>
                                    <span>For Sale</span>
                                </div>
                                <div className="listing-img-content">
                                    <span className="listing-compact-title">House Luxury <i>San Francisco</i></span>
                                    <ul className="listing-hidden-content">
                                        <li>Area <span>720 sq ft</span></li>
                                        <li>Rooms <span>6</span></li>
                                        <li>Beds <span>2</span></li>
                                        <li>Baths <span>3</span></li>
                                    </ul>
                                </div>
                                <img src={image3} alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="agents-grid mr-0">
                        <div className="listing-item compact">
                            <a href="properties-details.html" className="listing-img-container">
                                <div className="listing-badges">
                                    <span className="featured">$ 6,500</span>
                                    <span className="rent">For Rent</span>
                                </div>
                                <div className="listing-img-content">
                                    <span className="listing-compact-title">House Luxury <i>Miami FL</i></span>
                                    <ul className="listing-hidden-content">
                                        <li>Area <span>720 sq ft</span></li>
                                        <li>Rooms <span>6</span></li>
                                        <li>Beds <span>2</span></li>
                                        <li>Baths <span>3</span></li>
                                    </ul>
                                </div>
                                <img src={image4} alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="agents-grid mr-0">
                        <div className="listing-item compact">
                            <a href="properties-details.html" className="listing-img-container">
                                <div className="listing-badges">
                                    <span className="featured">$ 230,000</span>
                                    <span>For Sale</span>
                                </div>
                                <div className="listing-img-content">
                                    <span className="listing-compact-title">House Luxury <i>Chicago IL</i></span>
                                    <ul className="listing-hidden-content">
                                        <li>Area <span>720 sq ft</span></li>
                                        <li>Rooms <span>6</span></li>
                                        <li>Beds <span>2</span></li>
                                        <li>Baths <span>3</span></li>
                                    </ul>
                                </div>
                                <img src={image5} alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="agents-grid mr-0">
                        <div className="listing-item compact">
                            <a href="properties-details.html" className="listing-img-container">
                                <div className="listing-badges">
                                    <span className="featured">$ 6,500</span>
                                    <span className="rent">For Rent</span>
                                </div>
                                <div className="listing-img-content">
                                    <span className="listing-compact-title">House Luxury <i>Toronto CA</i></span>
                                    <ul className="listing-hidden-content">
                                        <li>Area <span>720 sq ft</span></li>
                                        <li>Rooms <span>6</span></li>
                                        <li>Beds <span>2</span></li>
                                        <li>Baths <span>3</span></li>
                                    </ul>
                                </div>
                                <img src={image6} alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FeatureProperties
