import React from 'react'

const Location = () => {
    return (
        <div className="property-location map">
            <h5>Location</h5>
            <div className="divider-fade"></div>
            <div id="map-contact" className="contact-map"></div>
        </div>

    )
}

export default Location
