import React, { useState, useEffect } from 'react'
import home1 from '../images/feature-properties/fp-1.jpg'
import house2 from '../images/feature-properties/fp-2.jpg'
import house3 from '../images/feature-properties/fp-3.jpg'
import Loader from "react-loader-spinner"
import { fetchPropertyData } from '../Api/index'
import { FacebookShareButton } from "react-share"

const SimilarProperties = () => {

    const [content, setContent] = useState([])

    useEffect(() => {

        const fetchApi = async () => {
            let data = await fetchPropertyData()
            data = data.slice(15, 18)
            setInterval(() => {
                setContent(data)
            }, 0)

        }
        fetchApi()

    }, [])
    // console.log(content)

    if (content.length === 0) {
        return (
            <div>
                <p>Wait a Moment...</p>
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={80}
                    width={80}
                    marginLeft={100}
                    id='loader'
                />
            </div>
        )
    }

    return (
        <div>
            {/* <section className="single-proper blog details">
            <div className="container"> */}
            <section className="similar-property recently portfolio bshd p-0 bg-white-inner">
                <div className="container">
                    <h5>Similar Properties</h5>

                    <div id='similar-properties'>


                        {content.map((contents) => (





                            
                                <div className="row featured portfolio-items">
                                    <div className="item col-lg-11 col-md-6 col-xs-12 landscapes">
                                        <div className="project-single">
                                            <div className="project-inner project-head">
                                                <div className="project-bottom">
                                                    <h4><a>View Property</a><span className="category">Real Estate</span></h4>
                                                </div>
                                                <div className="homes">

                                                    <a className="homes-img">
                                                        <div className="homes-tag button alt featured">Featured</div>
                                                        <div className="homes-tag button alt sale">For Sale</div>
                                                        <div className="homes-price">Family Home</div>
                                                        <img src={home1} alt="home-1" className="img-responsive" />
                                                    </a>
                                                </div>
                                                <div className="button-effect">
                                                    <a className="btn"><i className="fa fa-link"></i></a>
                                                    <a className="btn popup-video popup-youtube"><i className="fas fa-video"></i></a>
                                                    <a className="img-poppu btn"><i className="fa fa-photo"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-10 col-md-12 homes-content pb-0 mb-44" id='api-detail'>

                                        <h3><a>{contents.name}</a></h3>
                                        <p className="homes-address mb-3">
                                            <a>
                                                <i className="fa fa-map-marker"></i><span>{contents.locality}</span>
                                            </a>
                                        </p>

                                        <ul className="homes-list clearfix">
                                            <li>
                                                <i className="fa fa-bed" aria-hidden="true"></i>
                                                <span>{contents.noOfBedrooms} Bedrooms</span>
                                            </li>
                                            <li>
                                                <i className="fa fa-bath" aria-hidden="true"></i>
                                                <span>{contents.noOfBathrooms} Bathrooms</span>
                                            </li>
                                            <li>
                                                <i className="fa fa-object-group" aria-hidden="true"></i>
                                                <span>{contents.plotArea} sq ft</span>
                                            </li>
                                            <li>
                                                <i className="fas fa-warehouse" aria-hidden="true"></i>
                                                <span>{contents.disclaimer}</span>
                                            </li>
                                        </ul>

                                        <div className="price-properties">
                                            <h3 className="title mt-3">
                                                <a>$ {contents.price}</a>
                                            </h3>
                                            <div className="compare">
                                                <a title="Compare">
                                                    <i className="fas fa-exchange-alt"></i>
                                                </a>
                                                {/* <a title="Share"> */}
                                                <FacebookShareButton
                                                    url={"http://www.facebook.com"}
                                                ></FacebookShareButton>
                                                {/* </a> */}
                                                <a title="Favorites">
                                                    <i className="fa fa-heart-o"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="footer">
                                            <a>
                                                <i className="fa fa-user"></i> {contents.city}
                                            </a>
                                            <span>
                                                <i className="fa fa-calendar"></i> {contents.status}
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                









                       

















                        ))}
                        </div>
                </div>
            </section>
            {/* </div>
        </section> */}
        </div>
    )
}

export default SimilarProperties
