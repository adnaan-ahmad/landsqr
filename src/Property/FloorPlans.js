import React from 'react'
import image from '../images/single-property/s-2.jpg'

const FloorPlans = () => {
    return (
        <div className="floor-plan property wprt-image-video w50 pro">
            <h5>Floor Plans</h5>
            <img alt="image" src={image} />
        </div>
    )
}

export default FloorPlans
