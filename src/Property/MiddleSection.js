import React from 'react'
import Gallery from './Gallery'
import Description from './Description'
import PropertyDetails from './PropertyDetails'
import FloorPlans from './FloorPlans'
import WhatsNearby from './WhatsNearby'
import PropertyVideo from './PropertyVideo'
import ScheduleTour from './ScheduleTour'
import AgentInfo from './AgentInfo'
import RecentProperties from './RecentProperties'
import PopularTags from './PopularTags'
import FeatureProperties from './FeatureProperties'

const MiddleSection = () => {
    return (
        <div className="row">
            <div className="col-lg-8 col-md-12 blog-pots">
                <div className="row">
                    <div className="col-md-12">
                        <section className="headings-2 pt-2">
                            <Gallery />
                            <Description />
                        </section>
                    </div>
                </div>
                <PropertyDetails />
                <PropertyVideo />
                <WhatsNearby />
                <FloorPlans />
                {/* <Location /> */}

            </div>

            <aside className="col-lg-4 col-md-12 car">
                <div className="single widget"><br/>
                    <ScheduleTour />
                    <div className="sidebar">
                        <AgentInfo />
                        
                        <div className="main-search-field-2">
                            <RecentProperties />
                            
                            <FeatureProperties />
                            <PopularTags />
                        </div>
                    </div>
                </div>
            </aside>

        </div>
    )
}

export default MiddleSection
