import React from 'react'

import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import Footer from '../Footer'
import MainSection from './MainSection'

const Property = () => {
    return (
        <div>
            <HeaderTop />
            <Navigation />
            <MainSection />
            <Footer />
            
        </div>
    )
}

export default Property
