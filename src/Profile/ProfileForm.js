import React from 'react'
import '../App.css'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import Footer from '../Footer'
import SectionHeading from '../SectionHeading'
import { FormControl, NativeSelect } from '@material-ui/core'
import { Formik } from 'formik'
import Select from "react-select"
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import axios from 'axios'
import ReactHtmlParser from 'react-html-parser'

class ProfileForm extends React.Component {

  blogStatus = [{ label: "Owner", value: 1 }, { label: "Agent", value: 2 }, { label: "Builder", value: 3 }]
  bhk = [{ label: "1", value: 1 }, { label: "2", value: 2 }, { label: "3", value: 3 }, { label: "4", value: 4 }, { label: "> 4", value: 5 }]
  propertyType = [{ label: "Residential", value: 1 }, { label: "Commercial", value: 2 }, { label: "Agricultural", value: 3 }]


  // constructor(props){
  //   super(props)
  //   this.state = {
  //     file: null
  //   }
  //   this.handleChange = this.handleChange.bind(this)
  // }

  // handleChange(event) {
  //   this.setState({
  //     file: URL.createObjectURL(event.target.files[0])
  //   })
  // }

  constructor(props) {
    super(props)

    this.state = {
      title: '',
      summary: '',
      content: '',
      postStatus: '',
      tags: '',
      blogWriter: '5fa646426a92833ef9799ca3'
    }
  }

  changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
	}

  changeTags = e => {
    // console.log(e.label)
    this.setState({ tags: e.label })
  }

  changePostStatus = e => {
    // console.log(e.label)
    this.setState({ postStatus: e.label })
  }

  changeContent = (e, editor) => {
    // console.log(e)
    const data = editor.getData()
    this.setState({ content : ReactHtmlParser(data)[0].props.children[0]})

  }

  submitHandler = e => {
    e.preventDefault()
    
    console.log(this.state)
    axios
      .post('https://landsqr.herokuapp.com/api/v1/blogs/post', this.state)
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const { title, summary, content, postStatus, tags } = this.state
    return (

      <div>
        
        <div class="inner-pages">

          <div id="wrapper">


            <section class="royal-add-property-area section_100">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">


                    <div class="single-add-property">
                      <h3>My Profile</h3>
                      <div class="property-form-group">
                        <Formik>
                        <form onSubmit={this.submitHandler}>
                        

                        <div class="row">
                            <div class="col-md-6">
                              <p>
                                <label for="title">Location</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={title} name="title" id="title" placeholder="Enter City, Locality or Project.." />
                              </p>
                            </div>
                        </div>
                        
                        <br/>
                        <h5><b>Your Requirement</b></h5>
                          <div class="row">


                          



                            <div class="col-lg-6 col-md-12">
                              <div class="dropdown faq-drop">
                                <label for="dropdownmissdcall">Property Type</label>

                                <Select
                                  
                                  id="country"
                                  className='post-status'
                                  isClearable={true}
                                  isSearchable={true}
                                  name="postStatus"
                                  options={this.propertyType}
                                  label={postStatus}
                                  onChange={this.changePostStatus}
                                />
                              </div>
                            </div>
                            
                            
                            <div class="col-lg-6 col-md-12">
                              <div class="dropdown faq-drop">
                                <label for="dropdowntype">BHK</label>

                                <Select
                                  id="country"
                                  name="tags"
                                  isClearable={true}
                                  isSearchable={true}

                                  options={this.bhk}
                                  label={tags}
                                  onChange={this.changeTags}
                                />
                              </div>
                            </div>

                            
                            
                            
                            
                            
                            {/* <div class="row">
                             */}
                            
                            <div class="col-lg-6 col-md-6">
                              <p>
                                <label for="title">Area (In Sqft)</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={title} name="title" id="title" placeholder="Enter Area in Sqft.." />
                              </p>
                            </div>
                          

                          
                            <div class="col-lg-6 col-md-6">
                              <p>
                                <label for="title">Budget</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={summary} name="summary" id="title" placeholder="Enter Budget.." />
                                
                              </p>
                            </div>
                            
                           
                           
                           {/* </div> */}

                          
                          
                          
                          
                          
                          
                          
                          
                          </div>
                          <br/>

                          {/* <h5><b>Property Location</b></h5>
                          <div class="row">
                            <div class="col-md-6">
                              <p>
                                <label for="title">City</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={title} name="title" id="title" placeholder="Enter City.." />
                              </p>
                            </div>
                          

                          
                            <div class="col-md-6">
                              <p>
                                <label for="title">Locality</label>
                                <input type="text" onChange={this.changeHandler} className='blog-form' value={summary} name="summary" id="title" placeholder="Enter Locality.." />
                              </p>
                            </div>
                           </div> */}

                          {/* <div class="row">
                            <div class="col-lg-12 col-md-12">
                              <p class="no-mb">
                                <label for="price">Blog Image :</label>
                                
                                <input type="file" onChange={this.handleChange} name="price" className="filetype" id="price"/><br/><br/>
                                <img id="target" src={this.state.file} id='formImage' />
                              </p>
                            </div>
                            
                          </div> */}
                          <br/>
                          <div class="add-property-button">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="prperty-submit-button">
                                  <button type="submit">Edit Profile</button>
                                </div>
                              </div>
                            </div>
                          </div>

                        </form>
                        </Formik>
                      </div>
                    </div>


                    {/* <div class="add-property-button">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="prperty-submit-button">
                          <button type="submit">Submit Blog Post</button>
                        </div>
                      </div>
                    </div>
                  </div> */}

                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        
      </div>

    )
  }
}

export default ProfileForm
