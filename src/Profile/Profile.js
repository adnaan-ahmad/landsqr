import React from 'react'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import ProfileForm from './ProfileForm'

const Profile = () => {
  return (
    <div className="App">
      <HeaderTop />
      <Navigation />
      <SectionHeading heading = 'My Profile'/>
      <ProfileForm />
      <Footer />
    </div>
  );
}

export default Profile
