import React, { useEffect, useState } from 'react'
import house1 from '../images/blog/b-1.jpg'
import house2 from '../images/blog/b-2.jpg'
import house3 from '../images/blog/b-3.jpg'
import house4 from '../images/blog/b-4.jpg'
import house5 from '../images/blog/b-5.jpg'
import house6 from '../images/blog/b-6.jpg'
import person1 from '../images/testimonials/ts-2.jpg'
import person2 from '../images/testimonials/ts-4.jpg'
import person3 from '../images/testimonials/ts-6.jpg'
import SearchBar from '../SearchBar'
import Category from '../Category'
import PopularTags from '../PopularTags'
import RecentPosts from '../RecentPosts'
import '../App.css'
import {fetchData} from '../Api/index'
import Loader from "react-loader-spinner"
import { Link } from 'react-router-dom'

const MainSection = () => {

    const [content, setContent] = useState([])

        useEffect(() => {

            const fetchApi = async () => {
                let data = await fetchData()
                // data = data.slice(3,9)
                setInterval(() => {
                    setContent(data)
                }, 0)
            }
            fetchApi()

        }, [])
            //console.log(content)
        // const array = [[house1,person1,content], [house2, person2,content], [house3, person3,content]]
    
        if (content.length === 0) {
            return (
                <div className="inner-pages">

                <div id="wrapper">
                    <section className="blog blog-section">
                        <div className="container">
                <p>Wait a Moment...</p>
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={80}
                    width={80}
                    marginLeft={100}
                    id='loader'
                />
                </div>
                
                </section>
                </div>
                </div>
            )
        }

    return (
        
        
        <div className="inner-pages">

            <div id="wrapper">
                <section className="blog blog-section">
                    <div className="container">
                        <div className="row">

                        
                            <div className="col-lg-9 col-md-12 col-xs-12">
                                <div className="row">
                                
                                {content.map((contents) => (
                                    
                                    <div className="col-md-6 col-xs-12" id='bottom'>
                                        <div className="news-item nomb">
                                            <Link to="blogdetail" className="news-img-link">
                                                <div className="news-item-img">
                                                    <img className="img-responsive" src={house1} alt="blog image" />
                                                </div>
                                            </Link>
                                            <div className="news-item-text">
                                                <Link to="blogdetail"><h3>{contents.title}</h3></Link>
                                                <div className="dates">
                                                    <span className="date">April 1, 2020 &nbsp;/</span>
                                                    <ul className="action-list pl-0">
                                                        <li className="action-item pl-2"><i className="fa fa-heart"></i> <span>306</span></li>
                                                        <li className="action-item"><i className="fa fa-comment"></i> <span>34</span></li>
                                                        <li className="action-item"><i className="fa fa-share-alt"></i> <span>122</span></li>
                                                    </ul>
                                                </div>
                                                <div className="news-item-descr big-news">
                                                    <p>{contents.content}</p>
                                                </div>
                                                <div className="news-item-bottom">
                                                    <Link to="blogdetail" className="news-link">Read more...</Link>
                                                    <div className="admin">
                                                        <p>By, Karl Smith</p>
                                                        <img src={person1} alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/><br/>
                                    </div>
                                    
                                ))}
                                    
                                </div>
                            </div>

                            <aside className="col-lg-3 col-md-12">
                                <div className="widget">
                                    <SearchBar />
                                    <Category />
                                    <PopularTags />
                                    <RecentPosts />
                                </div>
                            </aside>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    )

}

export default MainSection
