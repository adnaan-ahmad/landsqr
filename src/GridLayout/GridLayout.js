import React from 'react'
import HeaderTop from '../HeaderTop'
import Navigation from '../Navigation'
import SectionHeading from '../SectionHeading'
import Footer from '../Footer'
import MainSection from './MainSection'


function BlogDetails() {
  return (
    <div className="App">
      <HeaderTop />
      <Navigation />
      <SectionHeading heading = 'Blog'/>
      <MainSection />
      <Footer />
    </div>
  );
}

export default BlogDetails
