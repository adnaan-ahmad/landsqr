import React from 'react'
import MainImage from '../images/blog/b-1.jpg'
import SearchBar from '../SearchBar'
import Category from '../Category'
import PopularTags from '../PopularTags'
import RecentPosts from '../RecentPosts'

function MainSection() {
    return (
        <div className="inner-pages">

            <div id="wrapper">
                <section className="blog blog-section bg-white">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-12 blog-pots">
                                <div className="row">
                                    <div className="col-md-12 col-xs-12">
                                        <div className="news-item details no-mb2">
                                            <a href="blog-details.html" className="news-img-link">
                                                <div className="news-item-img">
                                                    <img className="img-responsive" src={MainImage} alt="blog image" />
                                                </div>
                                            </a>
                                            <div className="news-item-text details pb-0">
                                                <a href="blog-details.html"><h3>Real Estate News</h3></a>
                                                <div className="dates">
                                                    <span className="date">April 11, 2020 &nbsp;/</span>
                                                    <ul className="action-list pl-0">
                                                        <li className="action-item pl-2"><i className="fa fa-heart"></i> <span>306</span></li>
                                                        <li className="action-item"><i className="fa fa-comment"></i> <span>34</span></li>
                                                        <li className="action-item"><i className="fa fa-share-alt"></i> <span>122</span></li>
                                                    </ul>
                                                </div>
                                                <div className="news-item-descr big-news details visib mb-0">
                                                    <p className="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, ea? Vitae pariatur ab amet iusto tempore neque a, deserunt eaque recusandae obcaecati eos atque delectus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi labore vel enim repellendus excepturi autem. Eligendi cum laboriosam exercitationem illum repudiandae quasi sint dicta consectetur porro fuga ea, perspiciatis aut!</p>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, ea? Vitae pariatur ab amet iusto tempore neque a, deserunt eaque recusandae obcaecati eos atque delectus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi labore vel enim repellendus excepturi autem. Eligendi cum laboriosam exercitationem illum repudiandae quasi sint dicta consectetur porro fuga ea, perspiciatis aut!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <aside className="col-lg-3 col-md-12">
                                <div className="widget">
                                    <SearchBar />
                                    <Category />
                                    <PopularTags />
                                    <RecentPosts />
                                </div>
                            </aside>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}

export default MainSection
