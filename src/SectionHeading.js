import React from 'react';

function App({ heading }) {
  return (
    <div className="inner-pages">
    {/* <!-- START SECTION HEADINGS -->
    <!-- Wrapper --> */}
    <div id="wrapper">
    
    <section className="headings">
            <div className="text-heading text-center">
                <div className="container">
                    <h1>{heading}</h1>
                    <h2><a href="index.html">Home </a> &nbsp;/&nbsp; {heading}</h2>
                </div>
            </div>
        </section>
    
    </div>
    </div>
  )
}

export default App
